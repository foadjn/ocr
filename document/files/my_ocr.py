import cv2 as cv
import numpy as np
import pdf2image
import pytesseract
from tqdm import tqdm


def line_sorter(element):
    return cv.boundingRect(element)[1]


def char_sorter(element):
    return cv.boundingRect(element)[0]


def remove_blank_lines(contours):
    for current_element in contours:
        x, y, w, h = cv.boundingRect(current_element)
        if h < 20:
            contours.remove(current_element)
    return contours


def main():
    # Read image
    doc = cv.imread('./files/doc.png')

    # Change rbg image to gray
    doc_gray = cv.cvtColor(doc, cv.COLOR_RGB2GRAY)

    # Apply median filter for noise reduction
    doc_gray = cv.GaussianBlur(doc_gray, (5, 5), 0)

    # Binarization to find contours in the image, and
    # get better results from Tesseract.
    _, doc_thresholded = cv.threshold(doc_gray, 0, 255,
                                      cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    # Dilate the image to get lines
    kernel = np.ones((5, 100), np.uint8)
    doc_dilation = cv.dilate(doc_thresholded, kernel, iterations=1)

    line_contours, _ = cv.findContours(image=doc_dilation,
                                       mode=cv.RETR_EXTERNAL,
                                       method=cv.CHAIN_APPROX_SIMPLE)

    # Sort contours to get lines top to bottom
    sorted_line_contours = sorted(line_contours,
                                  key=line_sorter)

    # Find each line in the doc, then,
    # apply find contours in a line to find chars
    recognized_text = ''
    for current_line_contour in tqdm(sorted_line_contours):
        x_line, y_line, w_line, h_line = cv.boundingRect(current_line_contour)
        # Extract current line
        current_line = doc_thresholded[
                       y_line:y_line + h_line, x_line:x_line + w_line]

        # Find characters in current line
        char_contours, _ = cv.findContours(image=current_line,
                                           mode=cv.RETR_EXTERNAL,
                                           method=cv.CHAIN_APPROX_SIMPLE)

        sorted_char_contours = sorted(char_contours,
                                      key=char_sorter)

        for current_char_contour in sorted_char_contours:
            x_char, y_char, w_char, h_char = cv.boundingRect(current_char_contour)

            # Extract character in current line
            current_char = current_line[
                           y_char:y_char + h_char, x_char:x_char + w_char]

            # Pad 0 to the current character to get better accuracy
            current_char = np.pad(current_char, 10, mode='constant',
                                  constant_values=0)

            # set -psm 10 config which determines it is only a character
            recognized_text = \
                recognized_text + pytesseract.image_to_string(current_char,
                                                              lang='eng',
                                                              config='-psm 10')
        # add new line
        recognized_text = recognized_text + "\n"

    result_file = open('./files/recognized.txt', "w+")
    result_file.write(recognized_text)
    result_file.close()


def extract_image():
    pages = pdf2image.convert_from_path(pdf_path='./files/doc.pdf',
                                        dpi=900)
    pages[0].save('./files/doc.png')


if __name__ == "__main__":
    extract_image()
    main()
